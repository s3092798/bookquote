package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn){
        HashMap<String, Double> result = new HashMap<>();
        result.put("1", 10.0);
        result.put("2", 45.0);
        result.put("3", 20.0);
        result.put("4", 35.0);
        result.put("5", 50.0);
        if(!isbn.equals("1") && !isbn.equals("2") && !isbn.equals("3") &&  !isbn.equals("4") && !isbn.equals("5")){
            result.put(isbn, 0.0);
        }
        return result.get(isbn);
    }
}
